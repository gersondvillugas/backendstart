import React, {Component} from 'react'
import styled from 'styled-components'
import Search from '../components/Search'
const ContForo = styled.div`
  display:flex;
  width: 50%;
  justify-content: space-around;
  background-color:#ffffff

`
const Imagen = styled.img`
  padding: 5px;
  width: 50%;
  height: 30%;
  
`
const Sugerencia = styled.div`
  margin-top:5px;
  display: flex;
  flex-wrap: wrap;
  ${'' /* height: 30px; */}
`

let  foros=[
    {  id:1213,
        titulo:"CUZCO",
        descripcion:"asdads",
        pic: "cuzo.jpg"

    },
    {   id:32323,
        titulo:"PUNO",
        descripcion:"asdasd",
        pic: "tingo.jpg"
    },
    {
        id:23232,
        titulo:"LIMA",
        descripcion:"asdasd",
        pic: "piura.jpg"

    },
    {
        id:546656,
        titulo:"CAJAMARCA",
        descripcion:"asdasd",
        pic: "arequipa.jpg"

    },
    {
        id:11212,
        titulo:"TRUJILLO",
        descripcion:"asdasd",
        pic: "cuzo.jpg"

    },
    {
        id:11212,
        titulo:"TRUJILLO",
        descripcion:"asdasd",
        pic: "cuzo.jpg"

    }
        
]

class EmpleadoAvatar extends React.Component {
  render() {
    return (
      <Imagen src={require(`../assets/imgs/${this.props.pic}`)} alt=""/>  
    )
  }
}
class ForoItem extends Component{
  render(){
    const {foro}=this.props
    return (
      <ContForo>
        <EmpleadoAvatar pic={foro.pic}/>
        <div  style = {{
          marginTop:'10px',
          marginBottom:'10px',
          backgroundColor:'#FFFF'
        }}>
          <p>  Id:   <strong> {foro.id} </strong></p>
          <p> <strong> Titulo: </strong>{foro.titulo}</p>
          <p> <strong>Descripcion:  </strong> {foro.descripcion} </p>
        </div>
      </ContForo>
    )
  }
}

export default class NLista extends Component{
  handleclick=(e)=>{
    alert('mela')   
  }
    render(){
      return(
        <div style = {{
          height: '100vh',
          overflow:'auto',
        }}>
          <Search/>
          <Sugerencia onClick={this.handleclick}>
            { foros.map(foro=>   
                <ForoItem foro={foro}/> 
            )}
          </Sugerencia>
        </div>
    )

    }
}